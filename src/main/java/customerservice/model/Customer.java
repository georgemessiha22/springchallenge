package customerservice.model;

public class Customer {

	/**
	 * Each Customer has a unique numeric ​ id ​ , ​ full name and ​ email ​ as
	 * fields
	 */
	private final long id;
	private String fullname;
	private String email;

	/**
	 * @param fullname
	 *            the fullname to set
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public Customer(long id, String fullname, String email) {
		this.id = id;
		this.fullname = fullname;
		this.email = email;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the fullname
	 */
	public String getFullname() {
		return fullname;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

}
