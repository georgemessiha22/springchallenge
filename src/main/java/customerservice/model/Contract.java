package customerservice.model;

public class Contract {

	/*
	 * Contract has a unique numeric ​ id ​ , the ​ id of the Customer it belongs
	 * to, a ​ start date ​ , a ​ type ​ and a monthly ​ revenue
	 */
	private final long id;
	private Customer customer;
	private String date;
	private String type;
	private double monthlyrevnue;

	public Contract(long id, Customer customer, String date, String type, double monthlyrevnue) {
		this.id = id;
		this.customer = customer;
		this.date = date;
		this.type = type;
		this.monthlyrevnue = monthlyrevnue;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the customer_id
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the monthlyrevnue
	 */
	public double getMonthlyrevnue() {
		return monthlyrevnue;
	}

}
