package customerservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import customerservice.model.Customer;
import customerservice.service.customer.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	CustomerService customerService;

	/**
	 * Get specific customer data using GET request big O is O(1)
	 * 
	 * @param customer_id:
	 *            the required customer id.
	 * @return Customer data in JSON object or NO_CONTENT OTHERWISE
	 */
	@RequestMapping(value = "/customerservice/customer/{customer_id}", method = RequestMethod.GET)
	public ResponseEntity<Customer> getCustomer(@PathVariable long customer_id) {
		Customer customer = customerService.findById(customer_id);
		if (customer == null) {
			return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	/**
	 * Saves in a new Customer big O is O(1)
	 * 
	 * @param customer_id:
	 *            the new customer id.
	 * @param customerJson:
	 *            the JSON object of the customer.
	 * @return CREATED or CONFLICT error otherwise.
	 */
	@RequestMapping(value = "/customerservice/customer/{customer_id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> createCustomer(@PathVariable long customer_id, @RequestBody CustomerJSON customerJson) {

		Customer customer = new Customer(customer_id, customerJson.fullname, customerJson.email);
		if (customerService.isCustomerExist(customer)) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		customerService.saveCustomer(customer);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

}

/**
 * creating a duplicate of Customer Model for JSON object only did this because
 * no SQL relations.
 * 
 * @author George Messiha
 *
 */
class CustomerJSON {
	String fullname;
	String email;

	public CustomerJSON() {

	}

	/**
	 * @param fullname
	 *            the fullname to set
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
