package customerservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import customerservice.model.Contract;
import customerservice.model.Customer;
import customerservice.service.contract.ContractService;
import customerservice.service.customer.CustomerService;

@RestController
public class ContractController {

	@Autowired
	ContractService contractService;
	@Autowired
	CustomerService customerservice;

	/**
	 * e.g /customerservice/contract/$customer_id big O is O(n)
	 * 
	 * @param customer_id
	 *            Id of customer need to be retrive it's data
	 * @return Sum of contracts monthly revenue for a customer. othewise NO_CONTENT
	 *         ERROR
	 */
	@RequestMapping(value = "/customerservice/contract/{customer_id:^[0-9]*$}", method = RequestMethod.GET)
	public ResponseEntity<Double> getContract(@PathVariable long customer_id) {
		List<Contract> contracts = contractService.findAllCustomerContracts(customer_id);
		if (contracts == null || contracts.isEmpty()) { // if there is no contracts for the customer
			return new ResponseEntity<Double>(HttpStatus.NO_CONTENT);
		}
		double wage = 0; // wage to sum in the contracts value
		for (Contract contract : contracts) {
			wage += contract.getMonthlyrevnue();
		}
		return new ResponseEntity<Double>(new Double(wage), HttpStatus.OK);
	}

	/**
	 * e.g /customerservice/contract/$type big O is O(1)
	 * 
	 * @param type
	 *            type of contracts.
	 * @return List of JSON objects contracts that has the given Type. otherwise
	 *         NO_CONTENT error
	 */
	@RequestMapping(value = "/customerservice/contract/{type:^[a-z]*$}", method = RequestMethod.GET)
	public ResponseEntity<List<Contract>> getContract(@PathVariable String type) {
		List<Contract> contracts = contractService.findAllTypeContracts(type);
		if (contracts.isEmpty()) {
			return new ResponseEntity<List<Contract>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Contract>>(contracts, HttpStatus.OK);
	}

	/**
	 * Add new Contract using PUT request e.g /customerservice/contract/$contract_id
	 * 
	 * @param contract_id
	 *            the new contract id
	 * @param contractJson
	 *            RequestBody data of the new Contract
	 * @return OK if added, error otherwise CONFLICT, NOT_FOUND if attached
	 *         customer_id is wrong.
	 */
	@RequestMapping(value = "/customerservice/contract/{contract_id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> createContract(@PathVariable long contract_id, @RequestBody ContractJSON contractJson) {

		if (contractService.findById(contract_id) != null) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		Customer customer = customerservice.findById(contractJson.customer_id);
		if (customer == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		Contract contract = new Contract(contract_id, customer, contractJson.date, contractJson.type,
				contractJson.monthlyrevnue);

		contractService.saveContract(contract);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
}

/**
 * creating a duplicate of Contract Model for JSON object only did this because
 * no SQL relations.
 * 
 * @author George Messiha
 *
 */
class ContractJSON {
	long customer_id;
	String date;
	String type;
	double monthlyrevnue;

	public ContractJSON() {

	}

	/**
	 * @param customer_id
	 *            the customer_id to set
	 */
	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param monthlyrevnue
	 *            the monthlyrevnue to set
	 */
	public void setMonthlyrevnue(double monthlyrevnue) {
		this.monthlyrevnue = monthlyrevnue;
	}
}
