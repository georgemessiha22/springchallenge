package customerservice.service.contract;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import customerservice.model.Contract;

@Service("contractService")
public class ContractServiceImpl implements ContractService {

	private static List<Contract> contracts = new ArrayList<Contract>();

	@Override
	public Contract findById(long id) {
		for (Contract contract : contracts) {
			if (contract.getId() == id) {
				return contract;
			}
		}
		return null;
	}

	@Override
	public List<Contract> findAllContracts() {
		// TODO Auto-generated method stub
		return contracts;
	}

	@Override
	public boolean isContractExist(Contract contract) {
		// TODO Auto-generated method stub
		for (Contract c : contracts) {
			if (contract.getId() == c.getId()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void saveContract(Contract contract) {
		contracts.add(contract);
	}

	@Override
	public List<Contract> findAllCustomerContracts(long customer_id) {
		if (contracts == null) {
			return null;
		}
		ArrayList<Contract> return_contracts = new ArrayList<Contract>();
		for (Contract contract : contracts) {
			if (contract.getCustomer().getId() == customer_id) {
				return_contracts.add(contract);
			}
		}
		return return_contracts;
	}

	@Override
	public List<Contract> findAllTypeContracts(String type) {
		if (contracts == null) {
			return null;
		}
		ArrayList<Contract> return_contracts = new ArrayList<Contract>();
		for (Contract contract : contracts) {
			if (contract.getType().equalsIgnoreCase(type)) {
				return_contracts.add(contract);
			}
		}
		return return_contracts;
	}

}
