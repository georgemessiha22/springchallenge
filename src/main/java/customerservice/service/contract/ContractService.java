package customerservice.service.contract;

import java.util.List;

import customerservice.model.Contract;

public interface ContractService {

	Contract findById(long id);

	void saveContract(Contract contract);

	List<Contract> findAllContracts();

	List<Contract> findAllCustomerContracts(long customer_id);

	public boolean isContractExist(Contract contract);

	List<Contract> findAllTypeContracts(String type);

}
