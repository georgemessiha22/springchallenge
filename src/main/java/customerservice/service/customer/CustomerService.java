package customerservice.service.customer;

import java.util.List;

import customerservice.model.Customer;

public interface CustomerService {

	Customer findById(long id);

	void saveCustomer(Customer customer);

	List<Customer> findAllCustomers();

	public boolean isCustomerExist(Customer customer);
}
