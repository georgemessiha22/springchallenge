package customerservice.service.customer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import customerservice.model.Customer;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

	private static final AtomicLong counter = new AtomicLong();

	private static List<Customer> Customers;

	static {
		Customers = populateDummyCustomers();
	}

	public List<Customer> findAllCustomers() {
		return Customers;
	}

	public Customer findById(long id) {
		for (Customer Customer : Customers) {
			if (Customer.getId() == id) {
				return Customer;
			}
		}
		return null;
	}

	public Customer findByName(String name) {
		for (Customer Customer : Customers) {
			if (Customer.getFullname().equalsIgnoreCase(name)) {
				return Customer;
			}
		}
		return null;
	}

	public void saveCustomer(Customer Customer) {
		Customers.add(Customer);
	}

	public boolean isCustomerExist(Customer Customer) {
		return findByName(Customer.getFullname()) != null;
	}

	private static List<Customer> populateDummyCustomers() {
		List<Customer> Customers = new ArrayList<Customer>();
		Customers.add(new Customer(counter.incrementAndGet(), "Sam", "@email.com"));
		Customers.add(new Customer(counter.incrementAndGet(), "Tom", "email.com"));
		Customers.add(new Customer(counter.incrementAndGet(), "Jerome", "email.com"));
		Customers.add(new Customer(counter.incrementAndGet(), "Silvia", "email.com"));
		return Customers;
	}

}
