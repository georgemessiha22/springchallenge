

**Environment Setup in this project:**
* Java
* Spring REST

##
This project was a test in Java and REST programming. in this README file you'll find the project describtion:

We would like you to implement a RESTful webservice that provides a small set of operationson a small scope model. The model is composed of customers and the sales contracts that they sign. So, for each Customer, we can have several unique Contract(s).
Each Customer has a unique numeric id, full name and email as fields. Contract has a unique numeric id, the id of the Customer it belongs to, a start date , a type and a monthly revenue, as fields. The service should implement and make available the following operations:
* ***add a new customer:***  
PUT /customerservice/customer/$customer_id  
the request body should contain all the necessary fields
* ***add a new contract to an existing customer:***  
PUT /customerservice/contract/$contract_id  
the request body should contain all the necessary fields
* ***retrieve all the information about an existing customer, including their contracts
information:***  
GET /customerservice/customer/$customer_id  
the response body should display all the required information
* ***retrieve the sum of revenues of all contracts from an existing customer:***  
GET /customerservice/contract/$customer_id  
the response body should contain a decimal with the total revenue of all contracts for
that customer
* ***retrieve the sum of revenues of all contracts of a specific type:***  
GET /customerservice/contract/$type  
the response body should contain a decimal with the revenue of the matching contracts  


### Requirements:
The I/O should be in JSON format. Do not use SQL. Saving the model in memory is fine (it is not in the scope of this challenge to concern the developer with persistent storage details), so use industry-standard data design patterns that can easily be extended to accommodate different persistence setups, and either a dummy/simulated DB or a simple local persistence mechanism. Justify any third-party libraries that you might have to resort to, but please keep them to a minimum. Aside from these requirements, any other considerations (error handling, formating, data structures, etc.) should be justified with proper code documentation. Please structure your project following clean separation-of-concerns and isolation patterns.  
Your project is not required to compile, so long as the intended operations behaviour is clear and documented in the code.  
As a bonus point, try to discuss the operations cost in terms of big O notation, and what considerations on this you might have. You have 12 hours to complete this challenge. Anything that you can not finish but have an idea of how to do it, can also be added as properly explained pseudo-code or workflow items. You can send the project back to us in a zip file, or upload it to an online public repository and
provide us with the URL.